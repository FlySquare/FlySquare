<!--
**FlySquare/FlySquare** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitHub profile. -->
### Hi there 👋


I was born in 2002 in Yalova. My passion brought me here. Since I couldn't stop my desire to create, I gathered information from every branch of hardware and software. I am actively trying to improve myself on Unity3D, Graphic Design, Database, Web Design and Desktop Programs. 

[![Linkedin Badge](https://img.shields.io/badge/flysquare-gray?style=for-the-badge&logo=linkedin)](https://www.linkedin.com/in/flysquare/)
[![Twitter Badge](https://img.shields.io/badge/flysquare0-gray?style=for-the-badge&logo=twitter)](https://twitter.com/flysquare0/)
[![Instagram Badge](https://img.shields.io/badge/fly.square-gray?style=for-the-badge&logo=instagram)](https://instagram.com/fly.square)


For more information about me, check out [bolfps.com](https://bolfps.com)

![FlySquare's GitHub Stats](https://github-readme-stats.vercel.app/api?username=flysquare&show_icons=true)

